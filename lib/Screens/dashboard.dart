import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

void main() {
  runApp(const Dashboard());
}

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);
  static const routeName = '/dashboard';
  @override
  State<Dashboard> createState() => _FindResState();
}

int currentIndex = 0;
final screens = [
  HomeScreen(),
  const Search_Screen(),
  const WishList(),
  const Account()
];

class _FindResState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "African Black Child",
          theme: ThemeData(
            backgroundColor: Colors.white,
            navigationBarTheme: NavigationBarThemeData(
              backgroundColor: Color.fromARGB(255, 255, 255, 255),
              indicatorColor: Colors.black,
            ),
            primaryTextTheme: Typography.blackRedmond,
            iconTheme: IconThemeData(color: Colors.black),
            buttonTheme: ButtonThemeData(buttonColor: Colors.black),
            colorScheme: const ColorScheme(
                primary: Colors.white,
                background: Color.fromARGB(255, 255, 255, 255),
                onPrimary: Colors.black,
                brightness: Brightness.light,
                onSecondary: Color.fromARGB(255, 255, 254, 254),
                error: Colors.red,
                onError: Colors.red,
                secondary: Color.fromARGB(255, 0, 0, 0),
                onBackground: Color.fromARGB(255, 0, 0, 0),
                onSurface: Color.fromARGB(255, 255, 253, 253),
                surface: Colors.black),
          ),
          home: Scaffold(
              body: screens[currentIndex],

              //Navigation Bar
              bottomNavigationBar: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  backgroundColor:
                      Theme.of(context).navigationBarTheme.backgroundColor,
                  fixedColor: Colors.black,
                  showUnselectedLabels: true,
                  currentIndex: currentIndex,
                  onTap: (index) => setState(() => currentIndex = index),
                  items: const <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                        icon: Icon(Icons.home), label: "Home"),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.search), label: "Search"),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.favorite), label: "Wish List"),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.person_outline_rounded,
                      ),
                      label: "Account",
                    )
                  ]),
              floatingActionButton: (currentIndex == 3)
                  ? FloatingActionButton(
                      onPressed: () {
                        FirebaseAuth.instance.signOut().then(
                            (value) => Navigator.pushNamed(context, '/login'));
                      },
                      child: const Icon(Icons.logout),
                    )
                  : FloatingActionButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('/checkout');
                      },
                      child: const Icon(Icons.shopping_cart_checkout),
                    )));
    });
  }
}

//Account screen
class Account extends StatefulWidget {
  const Account({Key? key}) : super(key: key);

  @override
  State<Account> createState() => _Profile_PageState();
}

class _Profile_PageState extends State<Account> {
  final TextEditingController _newEmail = TextEditingController();

  void updatingEmail() {
    FirebaseAuth.instance.currentUser!.updateEmail(_newEmail.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Row(children: <Widget>[
          const Icon(
            Icons.person,
          ),
          Text(
            "Edit Profile",
            style: Theme.of(context).textTheme.headline6,
          ),
        ], mainAxisAlignment: MainAxisAlignment.start),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: 200,
                    height: 200,
                    padding: EdgeInsets.all(3),
                    child: Image.asset(
                      "assets/user.jpg",
                    ))
              ],
            ),
            Divider(
              color: Colors.white,
              height: MediaQuery.of(context).size.width * 0.05,
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: TextField(
                  controller: _newEmail,
                  decoration: InputDecoration(
                    labelText: "Enter New email",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.all(20),
                  )),
            ),
            const Divider(
              color: Colors.white,
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      FirebaseAuth.instance.currentUser!
                          .updateEmail(_newEmail.text)
                          .then((value) =>
                              Navigator.pushNamed(context, '/login'));
                    },
                    child: Text("Update"),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          key: Key('button'),
          onPressed: () {
            FirebaseAuth.instance
                .signOut()
                .then((value) => Navigator.pushNamed(context, '/login'));
          },
          child: Icon(Icons.logout)),
    );
  }
}

//HOME SCREEN
class Catagories {
  Catagories(this.catagory_header, this.image_location);
  String catagory_header;
  String image_location;
}

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);
  @override
  State<HomeScreen> createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  final List<Catagories> _Catagories = [
    Catagories('Sweaters', 'assets/sweat.png'),
    Catagories('Hoodies', 'assets/hoodie.jpg'),
    Catagories('Sweaters', 'assets/sweat.png'),
    Catagories('Hoodies', 'assets/hoodie.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Clothing Store",
        ),
        elevation: 0,
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: ImageSlideshow(
                        children: [
                          Image.asset('assets/sale.png'),
                          Image.asset('assets/sales banner.jpg')
                        ],
                        initialPage: 0,
                        autoPlayInterval: 3000,
                        indicatorColor: Colors.white,
                        isLoop: true),
                    color: Color.fromARGB(255, 52, 57, 61),
                  ),
                ),
              ],
            ),
            Divider(),
            Expanded(
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 3,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: _Catagories.length,
                itemBuilder: (BuildContext ctx, index) {
                  return GridTile(
                    key: Key(_Catagories[index].catagory_header),
                    child: Image.asset(_Catagories[index].image_location),
                    header: Text(
                      _Catagories[index].catagory_header,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  );
                },
              ),
            )
          ]),
    );
  }
}

class Search_Screen extends StatefulWidget {
  const Search_Screen({Key? key}) : super(key: key);

  @override
  State<Search_Screen> createState() => _Search_ScreenState();
}

class _Search_ScreenState extends State<Search_Screen> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text("Search"),
      actions: [
        IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Searchbar()),
              );
            },
            icon: Icon(Icons.search))
      ],
    );
  }
}

class Searchbar extends StatefulWidget {
  const Searchbar({Key? key}) : super(key: key);

  @override
  State<Searchbar> createState() => _SearchbarState();
}

class _SearchbarState extends State<Searchbar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      actions: [
        Expanded(
          child: const TextField(
            toolbarOptions: ToolbarOptions(
                copy: true, paste: true, selectAll: true, cut: true),
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
              hintText: "Search",
              iconColor: Colors.black,
            ),
          ),
        )
      ],
    ));
  }
}

class WishList extends StatelessWidget {
  const WishList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Text(
              "Wish List",
              style: Theme.of(context).textTheme.headline6,
            ),
          ],
        ),
        actions: [],
        elevation: 0,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: ListView(children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                width: 300,
                height: 400,
                child: Image.asset("assets/hoodie.jpg")),
            Text(
              "ZAR 500",
              style: Theme.of(context).textTheme.headline5,
            )
          ],
        ),
      ]),
    );
  }
}
