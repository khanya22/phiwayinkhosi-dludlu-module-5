import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Screens/dashboard.dart';
import 'package:myapp/Screens/signup.dart';

void main() => runApp(Login());

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  //Textfield Controllers for Login page
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "African Black Child",
        home: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(15),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Image.asset(
                        "assets/logo.jpg",
                        height: 100,
                        width: 100,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextField(
                        controller: _emailController,
                        decoration: InputDecoration(
                            labelText: 'Enter Email',
                            alignLabelWithHint: true,
                            border: OutlineInputBorder()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextField(
                        controller: _passwordController,
                        decoration: const InputDecoration(
                            labelText: "Enter Password",
                            border: OutlineInputBorder()),
                        obscureText: true,
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          //Navigate to dashboard.dart
                          FirebaseAuth.instance
                              .signInWithEmailAndPassword(
                                  email: _emailController.text,
                                  password: _passwordController.text)
                              .then((value) =>
                                  Navigator.pushNamed(context, '/dashboard'));
                        },
                        style: ElevatedButton.styleFrom(primary: Colors.black),
                        child: const Text("Login",
                            style: TextStyle(color: Colors.white))),
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgotPassword()));
                        },
                        child: Text("Forgot Password?"))
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
              ),
            ),
          ),
          extendBody: false,
          backgroundColor: Colors.white,
          persistentFooterButtons: [
            TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/signup');
                },
                child: const Text(
                  "Sign Up",
                  style: TextStyle(color: Color.fromARGB(255, 8, 43, 199)),
                )),
          ],
        ));
  }
}

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();

  _changePassword() {
    FirebaseAuth.instance
        .sendPasswordResetEmail(email: _emailController.text)
        .then((value) => Navigator.pop(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reset Password"),
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.all(15),
          child: TextField(
              controller: _emailController,
              decoration: InputDecoration(
                label: Text("Email"),
                icon: Icon(
                  Icons.email,
                  color: Colors.black,
                ),
                labelStyle: TextStyle(color: Colors.black),
                focusedBorder: UnderlineInputBorder(),
              )),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
            onPressed: () {
              _changePassword();
            },
            child: Text("Forgot password"),
            style: ButtonStyle(),
          ),
        )
      ]),
    );
  }
}
