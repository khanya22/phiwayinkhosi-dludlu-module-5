import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  SignUp({Key? key}) : super(key: key);
  static const routeName = '/sigup';

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  //Textfield controller for handling text inputs
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Signup",
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text("Sign Up"),
            titleTextStyle: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
            leading: const Icon(
              Icons.person_rounded,
              color: Colors.black,
              size: 25,
            ),
            backgroundColor: Colors.white,
            elevation: 0,
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                      controller: _userNameController,
                      decoration: InputDecoration(
                          label: Text("Username"), icon: Icon(Icons.person))),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                      controller: _emailController,
                      decoration: InputDecoration(
                          label: Text("Email"), icon: Icon(Icons.email))),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                        label: Text("Password"),
                        icon: Icon(
                          Icons.password,
                        )),
                    obscureText: true,
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(15),
                    child: TextField(
                      decoration: InputDecoration(
                        label: Text("Confrim Password"),
                        icon: Icon(Icons.password),
                      ),
                      obscureText: true,
                    )),
              ],
              mainAxisSize: MainAxisSize.min,
            ),
          ),
          persistentFooterButtons: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/login');
                },
                child: const Text(
                  "<Back",
                  style: TextStyle(decoration: TextDecoration.underline),
                )),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                  onPressed: () {
                    FirebaseAuth.instance
                        .createUserWithEmailAndPassword(
                            email: _emailController.text,
                            password: _passwordController.text)
                        .then((value) =>
                            Navigator.pushNamed(context, '/dashboard'))
                        .onError((error, stackTrace) => null);
                  },
                  child: const Text(
                    "Confirm",
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  style: ButtonStyle(alignment: Alignment.centerLeft)),
            )
          ],
        ));
  }
}
