import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:myapp/Screens/checkout_screen.dart';
import 'package:myapp/Screens/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Screens/login.dart';
import 'package:myapp/Screens/signup.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Firebase.initializeApp();
  runApp(const African_Black_Child());
  FlutterNativeSplash.remove();
}

class African_Black_Child extends StatelessWidget {
  const African_Black_Child({Key? key}) : super(key: key);

  @override
  _isSignedIn() {
    if (FirebaseAuth.instance.currentUser?.uid == null) {
      return false;
    } else {
// logged

      return true;
    }
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: Colors.white,
          secondary: Colors.black,
          onPrimary: Colors.black,
          background: Colors.white,
          onBackground: Colors.black,
          outline: Colors.black,
          surface: Colors.white,
          onSurface: Colors.black,
          tertiary: Colors.black,
        ),
        appBarTheme: AppBarTheme(elevation: 0),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      title: 'African Black Child',
      debugShowCheckedModeBanner: false,
      //animated splash screen
      home: (_isSignedIn() == false) ? Login() : Dashboard(),

      //Routing table
      routes: {
        Dashboard.routeName: (ctx) => const Dashboard(),
        '/signup': (_) => SignUp(),
        '/login': (_) => Login(),
        '/checkout': (_) => const Checkout(),
        '/searchbar': (_) => const Searchbar()
      },
    );
  }
}
